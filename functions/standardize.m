function [var, center, scale] = standardize(var)
    center = nanmean(var);
    scale = nanstd(var);
    var = (var-center)/scale;
end

